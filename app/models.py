from datetime import datetime
from app import db, console


class TimestampMixin(object):
    created_at = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, onupdate=datetime.utcnow)


class User(TimestampMixin, db.Model):
    __tablename__ = 'users'
    ID = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.Text(), nullable=False)
    username = db.Column(db.Text(), nullable=False)
    password = db.Column(db.Text(), nullable=False)

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)

    def insert(self):
        user = User.query.filter_by(email=self.email, username=self.username).first()
        if not user:
            # Delete old users.
            item = User.query.first()
            if item:
                db.session.delete(item)
                db.session.commit()

            # # Create new user.
            db.session.add(self)
            db.session.commit()
            return console(f"User | {self.username} | has been created!")
        else:
            db.session.commit()
            return console(f"User | {self.username} | has been updated!")


class Quote(TimestampMixin, db.Model):
    __tablename__ = 'quotes'
    ID = db.Column(db.Integer, primary_key=True, autoincrement=True)
    quotes = db.Column(db.Text(), index=True, unique=True, nullable=False)
    title = db.Column(db.Text(), default=quotes)
    description = db.Column(db.Text(), default=title)
    author = db.Column(db.Text())
    link = db.Column(db.Text())
    background_color = db.Column(db.Text())
    text_color = db.Column(db.Text())
    logo = db.Column(db.Integer(), default=0)
    image_path = db.Column(db.Text())
    data_path = db.Column(db.Text())
    search_term = db.Column(db.Text(), nullable=False)
    board_id = db.Column(db.Text(), nullable=False)
    col_board_ids = db.Column(db.Text())

    def __init__(self, **kwargs):
        super(Quote, self).__init__(**kwargs)

    def insert(self):
        quote = Quote.query.filter_by(quotes=self.quotes).first()
        if not quote:
            db.session.add(self)
            db.session.commit()

            return console(f"Quote | {self.quotes} | has been created!")

    def update(self, kwargs):
        Quote.query.filter_by(ID=self.ID).update(kwargs)
        db.session.commit()
        return console(f"Quote | ID: {self.ID} | has been updated!")

    def delete(self):
        item = Quote.query.get(self.ID)
        db.session.delete(item)
        db.session.commit()
        return console(f"Quote | {item.quotes} | has been deleted!")


class Pin(TimestampMixin, db.Model):
    __tablename__ = 'pins'
    ID = db.Column(db.Integer, primary_key=True, autoincrement=True)
    pin_id = db.Column(db.Integer(), index=True, unique=True, nullable=False)
    search_term = db.Column(db.Text(), index=True)
    board_id = db.Column(db.Text(), index=True)
    status = db.Column(db.Integer(), default=0)
    type_pin = db.Column(db.Text(), default='rePin')
    is_unique = db.Column(db.Integer(), default=0)
    is_collaboration = db.Column(db.Integer(), default=0)
    link = db.Column(db.Text())
    title = db.Column(db.Text())
    description = db.Column(db.Text())
    col_board_ids = db.Column(db.Text())

    def __init__(self, **kwargs):
        super(Pin, self).__init__(**kwargs)

    def insert(self):
        pin = Pin.query.filter_by(pin_id=self.pin_id).first()
        if not pin:
            db.session.add(self)
            db.session.commit()

            return console(f"Pin | pin_id: {self.pin_id} | has been created!")

    def update(self, kwargs):
        Pin.query.filter_by(pin_id=self.pin_id).update(kwargs)
        db.session.commit()
        return console(f"Pin | Pin ID: {self.pin_id} | has been updated!")

    def delete(self):
        item = Pin.query.get(self.ID)
        db.session.delete(item)
        db.session.commit()
        return console(f"Pin | Pin ID: {item.pin_id} | has been deleted!")


class Board(TimestampMixin, db.Model):
    __tablename__ = 'boards'
    ID = db.Column(db.Integer, primary_key=True, autoincrement=True)
    board_id = db.Column(db.Text(), index=True, unique=True)
    name = db.Column(db.Text())
    is_collaboration = db.Column(db.Integer(), default=0)

    def __init__(self, **kwargs):
        super(Board, self).__init__(**kwargs)

    def insert(self):
        board = Board.query.filter_by(board_id=self.board_id).first()
        if not board:
            db.session.add(self)
            db.session.commit()

            return console(f"Board | {self.name} | board_id: {self.board_id} | has been created!")

    def update(self, kwargs):
        Board.query.filter_by(ID=self.ID).update(kwargs)
        db.session.commit()
        return console(f"Board | ID: {self.ID} | has been updated!")

    def delete(self):
        item = Board.query.get(self.ID)
        db.session.delete(item)
        db.session.commit()
        return console(f"Board | ID: {item.ID} | has been deleted!")


class Follow(TimestampMixin, db.Model):
    __tablename__ = 'follows'
    ID = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Text, index=True)
    we_follow = db.Column(db.Integer, default=0)
    they_follow = db.Column(db.Integer, default=0)

    def __init__(self, **kwargs):
        super(Follow, self).__init__(**kwargs)

    def insert(self):
        db.session.add(self)
        db.session.commit()
        return console(f"Follow | user_id: {self.user_id} | has been created!")

    def update(self, kwargs):
        Follow.query.filter_by(user_id=self.user_id).update(kwargs)
        db.session.commit()
        return console(f"Follow | user_id: {self.user_id} | has been updated!")

    def delete(self):
        item = Follow.query.get(self.ID)
        db.session.delete(item)
        db.session.commit()
        return console(f"Follow | user_id: {item.user_id} | has been deleted!")
