import logging

from flask import Flask
from flask_socketio import SocketIO
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
from app.config import Config

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
cors = CORS(app)
migrate = Migrate(app, db)
socketio = SocketIO(app)

if __name__ != '__main__':
    # create logger
    logging.basicConfig(filename='console.log', format="[%(asctime)s] - [%(levelname)s] : [%(message)s]")
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
    # create console handler and set level to debug
    stream = logging.StreamHandler()
    stream.setLevel(logging.DEBUG)


def console(*args):
    """
    Function for logging.
    """
    # Print on the development environment

    log = " ".join(map(str, args))
    print(log)
    if __name__ != '__main__':
        app.logger.info(log)

        with open(Config.LOG_FILE, 'r') as f:
            log_buffer = f.readlines()
            log_buffer.reverse()
        socketio.emit('my response', log_buffer[0])

    return log


from app import routes, models
from app.services import utils

utils.create_uploads_folder_structure(Config.FOLDER_LIST)
