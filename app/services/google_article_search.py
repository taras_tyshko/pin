import json
import os
import uuid

from packages.google import google
from app.models import Pin
from app import pin, console
from app.config import Config
from app.services import quote

RESULTS_PER_PAGE = 70


def search(google_query, max_amount, board_id, col_board_ids, descr_prefix, google_descr_length):
    if max_amount % RESULTS_PER_PAGE == 0:
        num_page = max_amount // RESULTS_PER_PAGE
    else:
        num_page = max_amount // RESULTS_PER_PAGE + 1

    console(google_query)
    search_results = google.search(google_query, num_page)
    if Config.STOP_THREAD:
        return
    console(len(search_results))

    results = {}
    links = []
    for result in search_results:
        link = result.link
        if link is not None:
            results[link] = result
            links.append(link)
    pins_in_db = Pin.query.filter_by(type_pin='google_article').with_entities(Pin.link).all()
    links_in_db = [el[0] for el in pins_in_db]

    counter = 0
    # articles_to_generate = pin.difference(links, links_in_db)
    articles_to_generate = list(set(links).difference(set(links_in_db)))

    for article in articles_to_generate:
        if counter < max_amount and not Config.STOP_THREAD:
            load_article = results[article]
            pin_id = str(uuid.uuid4())
            description = create_description(load_article.description, descr_prefix, google_descr_length)
            pin_title = load_article.name[:Config.PIN_TITLE_MAX_LENGTH]
            Pin(pin_id=pin_id, search_term=google_query, board_id=board_id, col_board_ids=col_board_ids,
                type_pin='google_article', link=load_article.link, title=pin_title, description=description).insert()
            pin_metadata = {'title': pin_title, 'description': description, 'board_id': board_id,
                            'col_board_ids': col_board_ids.split(','),
                            'link': load_article.link}
            pin_folder_name = os.path.join(Config.GOOGLE_ARTICLES_PINS_FOLDER, pin_id)
            os.makedirs(pin_folder_name)
            f = open(os.path.join(pin_folder_name, 'data.json'), 'a')
            f.write(json.dumps(pin_metadata, indent=4, sort_keys=False))
            f.close()
            # create an image
            image_path = os.path.join(pin_folder_name, 'image.png')
            quote.quotes(desc=load_article.name, bc_rgb=quote.is_background_color(''), logo=quote.is_logo('TRUE'),
                         font=quote.is_font(''), color_text=quote.is_text_color(''),
                         author=quote.is_author(''), path=image_path)
            counter += 1
        else:
            break


def create_description(initial_description, descr_prefix, google_descr_length):
    if '\u0440. - ' in initial_description:
        description = initial_description.split('\u0440. - ')[1]
    else:
        description = initial_description
    full_descr = descr_prefix + description[:google_descr_length] + '...'

    return full_descr[:Config.PIN_DESCR_MAX_LENGTH]

    # if len(pins) == 0:
    #     # we insert the pin into a database and create a folder + file with pin's metadata
    #     pin_id = str(uuid.uuid4())
    #     Pin(pin_id=pin_id, search_term=google_query, board_id=board_id,
    #         type_pin='google_article', link=link, title=title, description=description).insert()
    #     pin_metadata = {'title': title, 'description': description, 'board_id': board_id, 'link': link}
    #     pin_folder_name = os.path.join(Config.GOOGLE_ARTICLES_PINS_FOLDER, pin_id)
    #     os.makedirs(pin_folder_name)
    #     f = open(os.path.join(pin_folder_name, 'data.json'), 'a')
    #     f.write(json.dumps(pin_metadata, indent=4, sort_keys=False))
    #     f.close()


# num_page = 2
# search_results = google.search(
#     "classic interior living room -modern -pinterest -shutterstock -facebook -youtube -depositphotos -aliexpress "
#     "-ebay -alibaba -amazon -apple -istockphoto",
#     num_page)
# for result in search_results:
#     # console(result.description)
#     console(result.name)
#     # console(type(result))

# creating a folder for search results if it does not exist


# search_google_articles('How to get motivated', 5, 10, '677017825172870176', '', 15)
