import json
import os

from app import console
from app.models import Pin
from google_images_search import GoogleImagesSearch
from app.config import Config

# === START CONSTANTS ===
PATH_TO_GOOGLE_IMAGES = Config.GOOGLE_IMAGES_PINS_FOLDER
# === END CONSTANTS ===

# if you don't enter api key and cx, the package will try to search
# them from environment variables GCS_DEVELOPER_KEY and GCS_CX
gis = GoogleImagesSearch('AIzaSyBKSKXXZQLDAz59C2acUcCTZizvWshot6A', '008484789061085334797:55m1pyt5pwo')

# define search params:
_search_params = {
    'q': 'design',

    'num': '8',
    'start': '1',
    # 'safe': 'medium',
    'fileType': 'jpeg,gif,png,jpg,svg',
    # 'imgType': 'photo',
    # 'imgSize': "large",
    # 'imgDominantColor': 'black|blue|brown|gray|green|pink|purple|teal|white|yellow'
}




# search first, then download and resize afterwards
def search_results(search_term, board_id, col_board_ids, total_count, description_prefix):
    _search_params["q"] = search_term
    # console(_search_params)
    results = []
    pins_in_db = Pin.query.filter_by(type_pin='google').with_entities(Pin.pin_id).all()
    pins_ids_db = set([el[0] for el in pins_in_db])
    f = open('db.txt', 'a')
    f.write(json.dumps(pins_in_db))
    f.close()
    gis.search(search_params=_search_params)
    if Config.STOP_THREAD:
        return
    batch = gis.results()
    while len(batch) > 0 and len(results) < int(total_count):
        if Config.STOP_THREAD:
            break
        image_links = set([el.url for el in batch])
        # console('image_links')
        # console(image_links)
        pins_to_create = image_links.difference(pins_ids_db)
        # console('pins_to_create')
        # console(pins_to_create)
        for image in batch:
            # console(image.title)
            if image.url in pins_to_create and os.path.splitext(image.url)[1] != '':
                if Config.STOP_THREAD:
                    break
                description = description_prefix + image.snippet
                Pin(pin_id=image.url, search_term=search_term, board_id=board_id, col_board_ids=col_board_ids,
                    type_pin='google', link=image.url, title=image.title[:Config.PIN_TITLE_MAX_LENGTH],
                    description=description[:Config.PIN_DESCR_MAX_LENGTH - 3] + '...').insert()
                pin_db_id = Pin.query.filter_by(pin_id=image.url).with_entities(Pin.ID).first()
                # to create a folder with id and a json with a metadata of a pin
                pin_metadata = {'title': image.title, 'description': image.snippet, 'board_id': board_id,
                                'col_board_ids': col_board_ids.split(','),
                                'link': image.url}
                pin_folder_name = PATH_TO_GOOGLE_IMAGES + str(pin_db_id[0])
                os.makedirs(pin_folder_name)
                f = open(os.path.join(pin_folder_name, 'data.json'), 'a')
                f.write(json.dumps(pin_metadata, indent=4, sort_keys=False))
                f.close()
                image.download(pin_folder_name)
                pins_ids_db.add(image.url)
                results.append(image.url)

        _search_params['start'] = str(int(_search_params['num']) + int(_search_params['start']))
        # console('_search_params')
        # console(_search_params)
        gis.search(search_params=_search_params)
        batch = gis.results()

