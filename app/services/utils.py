import os
import shutil
from zipfile import ZipFile

from app import console
from app.config import Config


def zip_extract(file_path, file_name):
    # opening the zip file in READ mode
    path = Config.UPLOAD_MY_PINS_FOLDER + os.path.splitext(file_name)[0]
    if os.path.exists(path):
        remove_dir(path)

    with ZipFile(file_path, 'r') as zip_file:
        for name in zip_file.namelist():
            if name.endswith('.DS_Store'):
                continue
            elif name.startswith('_'):
                continue
            zip_file.extract(name, Config.UPLOAD_MY_PINS_FOLDER)

    remove_files(file_path)


def create_dir(path):
    if not os.path.exists(path):
        try:
            os.mkdir(path)
            console(f'Directory | {path} | has been created!')
            return path
        except OSError:
            return console("Error while creating directory ", path)


def mkdir(path):
    if not os.path.exists(path):
        try:
            os.mkdir(path)
            console(f'Directory | {path} | has been created!')
            return path
        except OSError as e:
            console(e)
            return console("Error while creating directory ", path)


def remove_dir(path):
    if os.path.exists(path):
        try:
            shutil.rmtree(path)
            return console(f'Directory | {path} | has been deleted!')
        except OSError:
            return console("Error while deleting directory ", path)


def remove_files(file_path):
    if os.path.exists(file_path):
        try:
            os.remove(file_path)
            return console(f'File | {file_path} | has been deleted!')
        except OSError as e:
            return console("Error while deleting file ", file_path, " ", e)


def create_uploads_folder_structure(list_folders):
    for folder_path in list_folders:
        create_dir(folder_path)
