import json
import os
from os import listdir
import uuid
from pathlib import Path
from random import randint
from app.models import Pin, Follow, Board
from app.config import Config
from . import console
from .services import database
from .services.pinterest import pinterest, user
from datetime import datetime, timedelta
from apscheduler.schedulers.blocking import BlockingScheduler
import numpy as np
import shutil
from app.services import utils

# TIME_RANGES =[[3, 6], [9, 11], [14, 18], [21, 23], [20, 22]]
TIME_RANGES = []
TIME_RANGES_FULL = {}

TIME_RANGES_FOLLOW = []
TIME_RANGES_FOLLOW_FULL = {}

PINS_AMOUNT_MAP = {}
FOLLOWS_AMOUNT_MAP = {}
IDS_TO_FOLLOW = {}
ALL_PINS = []

SCHEDULE = BlockingScheduler(job_defaults={'misfire_grace_time': 15 * 60})

SEARCH_TERMS_TO_INCLUDE_ARR = []
SEARCH_TERMS_TO_EXCLUDE_ARR = []
SEARCH_TERMS_TO_INCLUDE = ''
SEARCH_TERMS_TO_EXCLUDE = ''

FOLDER_NAME_TO_ID = {}

POST_TO_COLLABORATIVE_BOARDS = False


def search(query, repin_count, board_id):
    """
    Implementation of Pinterest search
    :param query:
    :param repin_count:
    :param board_id:
    """
    # After change in pinterest API, you can no longer search for users
    # Instead you need to search for something else and extract the user data from there.
    # current pinterest scopes are: pins, buyable_pins, my_pins, videos, boards
    results = []
    ids = []
    pins_in_db = []
    # Getting pin "ID" from database
    # Getting of Pinterest pins
    for item in Pin.query.all():
        pins_in_db.append(item.pin_id)
    open('logs/db_pins.json', 'w').write(json.dumps(pins_in_db))

    search_batch = pinterest.search(scope='pins', query=query)
    while len(search_batch) > 0 and len(ids) < 700:
        for pin in search_batch:
            if Config.STOP_THREAD:
                break
            # Checking type pin
            if pin["type"] == "pin":
                # Getting and add pin "ID"
                ids.append(pin["id"])

        # Getting next pins
        search_batch = pinterest.search(scope='pins', query=query)
    if Config.STOP_THREAD:
        return "Search was cancelled"
    else:
        open('logs/search_pins.json', 'w').write(json.dumps(ids))
        # Filter and set unique pin "ID" to results variable
        # results += difference(ids, pins_in_db)
        # results = list(dict.fromkeys(results))
        results = list(set(ids).difference(set(pins_in_db)))
        # remove duplicate pins if such exist
        open('logs/results.json', 'w').write(json.dumps(results))
        get_pin_id(results, query, repin_count, board_id, pins_in_db)


def list_board_ids(username):
    output_boards = []

    boards = pinterest.boards(username=username)
    console(username)
    for board in boards:
        output_boards.append({'name': board['name'], 'id': board['id']})
    return output_boards


def follow(count, username_followers, days_amount, time_ranges):
    if count is not None and days_amount is not None and username_followers != '' and time_ranges != '':
        process_time_ranges(time_ranges, TIME_RANGES_FOLLOW)

        users_result = get_followers(username_followers, count, 5)
        i = 0

        console('Ids to follow', IDS_TO_FOLLOW)
        if count > len(IDS_TO_FOLLOW):
            console('There are not enough users to follow - ,', count, ', available users amount - ',
                    len(IDS_TO_FOLLOW))
            count = len(IDS_TO_FOLLOW)
        if len(IDS_TO_FOLLOW) > 0:
            # def scheduleActivity(amount, targetMap, time_to_schedule, time_to_schedule_full, method, arguments,
            # daysAmount):
            schedule_activity(count, FOLLOWS_AMOUNT_MAP, TIME_RANGES_FOLLOW, TIME_RANGES_FOLLOW_FULL, follow_single,
                              IDS_TO_FOLLOW, days_amount)


def get_followers(username, max_items, max_batch_amount):
    console('MAX ITEMS TO FOLLOW=', max_items)
    followers = []
    new_followers = []
    followers_batch = pinterest.get_user_followers(username=username)
    batch_counter = 0
    i = 0

    while len(followers_batch) > 0 and len(IDS_TO_FOLLOW) < max_items and batch_counter < max_batch_amount:
        # console('followers_batch', followers_batch)
        new_followers = followers_batch
        for follower in new_followers:
            # check if we already follow the user
            if not database.check_if_follow(follower['id']):
                IDS_TO_FOLLOW[i] = [follower['id']]
                i += 1
                # console('follower=', follower)
        # followers += followers_batch
        followers += new_followers
        followers_batch = pinterest.get_user_followers(username=username)
        batch_counter += 1

    return followers


def follow_single(user_id):
    request = pinterest.follow_user(user_id=user_id)

    if request.status_code == 200:
        Follow(user_id=user_id, we_follow=1).insert()


def get_pin_id(pins, search_term, re_pin_count, board_id, pins_db):
    """
    Getting pin from Pinterest and check if param "repin_count" > re_pin_count. And save in database.
    :param pins_db:
    :param pins:
    :param search_term:
    :param re_pin_count:
    :param board_id:
    """
    for pin in pins:
        try:
            if Config.STOP_THREAD:
                break
            # Getting data from Pinterest
            new_pin = pinterest.load_pin(pin_id=pin)
            # Check whether the pin has the right amount
            if new_pin['pins'][f'{pin}']['repin_count'] >= re_pin_count and new_pin["pins"][pin]["id"] not in pins_db:
                console('Creating a pin ', pin)
                Pin(pin_id=f'{new_pin["pins"][pin]["id"]}', search_term=search_term, board_id=board_id).insert()
        except:
            pass
    console('All the pins have been checked!')


def re_pins(count, board_id, search_term):
    """
    Function implementation rePin on Pinterest.
    :param count:
    :param board_id:
    :param search_term:
    """
    # Getting pins form database
    pins = Pin.query.filter_by(search_term=search_term, status=0).limit(count).all()
    console(pins)
    for pin in pins:
        # Do repin on the Pinterest
        request = pinterest.repin(board_id=board_id, pin_id=pin.pin_id)
        # Check if request is OK
        if request.status_code == 200:
            # Update status in database for current pin
            Pin(pin_id=pin.pin_id).update(dict(status=1))
        else:
            pass


def re_pin_single(pin_id, board_id):
    # Do repin on the Pinterest
    request = pinterest.repin(board_id=board_id, pin_id=pin_id)
    # Check if request is OK
    if request.status_code == 200:
        # Update status in database for current pin
        Pin(pin_id=pin_id).update(dict(status=1))
    else:
        pass


def stop_scheduled_jobs():
    global SCHEDULE
    if SCHEDULE.running:
        console('Stopping schedule set')
        SCHEDULE.shutdown()
        reset_global_params()
    else:
        console('Please wait scheduler to finish preparing the scheduler, then stop the job if needed')
        # ?if needs to be done, we stop everything anyway? - we do this when we stop all
        # reset_global_params()


def reset_global_params():
    # resetting the params
    Config.STOP_THREAD = False

    global TIME_RANGES
    TIME_RANGES = []
    global TIME_RANGES_FULL
    TIME_RANGES_FULL = {}
    global TIME_RANGES_FOLLOW
    TIME_RANGES_FOLLOW = []
    global TIME_RANGES_FOLLOW_FULL
    TIME_RANGES_FOLLOW_FULL = {}
    global PINS_AMOUNT_MAP
    PINS_AMOUNT_MAP = {}
    global FOLLOWS_AMOUNT_MAP
    FOLLOWS_AMOUNT_MAP = {}
    global IDS_TO_FOLLOW
    IDS_TO_FOLLOW = {}
    global ALL_PINS
    ALL_PINS = []
    global SEARCH_TERMS_TO_INCLUDE_ARR
    SEARCH_TERMS_TO_INCLUDE_ARR = []
    global SEARCH_TERMS_TO_EXCLUDE_ARR
    SEARCH_TERMS_TO_EXCLUDE_ARR = []
    global SEARCH_TERMS_TO_INCLUDE
    SEARCH_TERMS_TO_INCLUDE = ''
    global SEARCH_TERMS_TO_EXCLUDE
    SEARCH_TERMS_TO_EXCLUDE = ''
    global FOLDER_NAME_TO_ID
    FOLDER_NAME_TO_ID = {}
    global POST_TO_COLLABORATIVE_BOARDS
    POST_TO_COLLABORATIVE_BOARDS = False


def check_parameters_schedule_set(days_amount, repin_amount, repin_count,
                                  google_articles_count, google_images_count, quotes_count,
                                  my_pins_count, time_ranges, post_to_coll):
    params = {'days_amount': days_amount,
              'repin_amount': repin_amount, 'repin_count': repin_count,
              'google_articles_count': google_articles_count,
              'google_images_count': google_images_count,
              'quotes_count': quotes_count,
              'my_pins_count': my_pins_count,
              'time_ranges': time_ranges,
              'post_to_coll': post_to_coll}
    all_params_set = True
    parameters_not_set = []
    for key in params:
        if params[key] is None or params[key] == '':
            all_params_set = False
            parameters_not_set.append(key)
    return [all_params_set, parameters_not_set]


def schedule_set(days_amount,
                 repin_amount, repin_count,
                 google_articles_count,
                 google_images_count,
                 quotes_count,
                 my_pins_count,
                 search_terms_to_include, search_terms_to_exclude,
                 time_ranges,
                 count_follow, days_amount_follow, username_followers, time_ranges_follow,
                 post_to_coll):
    # stopping the existing scheduler if such exists
    global SCHEDULE
    if SCHEDULE.running:
        SCHEDULE.shutdown()
        reset_global_params()
        SCHEDULE = BlockingScheduler(job_defaults={'misfire_grace_time': 15 * 60})
    # check the parameters
    params_result = check_parameters_schedule_set(days_amount,
                                                  repin_amount, repin_count,
                                                  google_articles_count,
                                                  google_images_count,
                                                  quotes_count,
                                                  my_pins_count,
                                                  time_ranges,
                                                  post_to_coll)
    if params_result[0]:
        process_time_ranges(time_ranges, TIME_RANGES)
        console('TIME_RANGES for Pins to be posted=', TIME_RANGES)
        global POST_TO_COLLABORATIVE_BOARDS
        POST_TO_COLLABORATIVE_BOARDS = post_to_coll
        # checking if we have enough pins of each type
        enough_all_pins = True
        pin_types_needed = []
        global SEARCH_TERMS_TO_INCLUDE
        global SEARCH_TERMS_TO_EXCLUDE
        global SEARCH_TERMS_TO_INCLUDE_ARR
        global SEARCH_TERMS_TO_EXCLUDE_ARR

        if search_terms_to_include != '' and search_terms_to_exclude != '':
            console("You can use only one parameter among search terms to include OR search terms to exclude values")
            return "You can use only one parameter among search terms to include OR search terms to exclude values"

        SEARCH_TERMS_TO_INCLUDE_ARR = search_terms_to_include.split(',')
        SEARCH_TERMS_TO_EXCLUDE_ARR = search_terms_to_exclude.split(',')
        SEARCH_TERMS_TO_INCLUDE = search_terms_to_include
        SEARCH_TERMS_TO_EXCLUDE = search_terms_to_exclude

        types_to_check = {'google_article': {'folder_name': Config.GOOGLE_ARTICLES_PINS_FOLDER,
                                             'pins_per_day': google_articles_count},
                          'quote': {'folder_name': Config.UPLOAD_QUOTES_FOLDER,
                                    'pins_per_day': quotes_count},
                          'my_pin': {'folder_name': Config.UPLOAD_MY_PINS_FOLDER,
                                     'pins_per_day': my_pins_count},
                          'google_image': {'folder_name': Config.GOOGLE_IMAGES_PINS_FOLDER,
                                           'pins_per_day': google_images_count}
                          }
        pins_in_db_include = []
        pins_in_db_exclude = []
        id_to_board_id = []

        if search_terms_to_include != '':
            console('search_terms_to_include')
            console(SEARCH_TERMS_TO_INCLUDE_ARR)
            pins_in_db_include = Pin.query.filter(Pin.search_term.in_(SEARCH_TERMS_TO_INCLUDE_ARR)).with_entities(
                Pin.pin_id, Pin.board_id).all()
            ids_to_include = [el[0] for el in pins_in_db_include]
            id_to_board_id = {el[0]: el[1] for el in pins_in_db_include}
        elif search_terms_to_exclude != '':
            console('search_terms_to_exclude')
            console(SEARCH_TERMS_TO_EXCLUDE_ARR)
            pins_in_db_exclude = Pin.query.filter(Pin.search_term.in_(SEARCH_TERMS_TO_EXCLUDE_ARR)).with_entities(
                Pin.pin_id).all()
            ids_to_exclude = [el[0] for el in pins_in_db_exclude]

        for key in types_to_check:
            value = types_to_check[key]

            ids = listdir(value['folder_name'])
            ids_we_can_use = set(ids)

            if (search_terms_to_include != '') and key in ('google_article', 'google_image'):
                ids_we_can_use = set(ids).intersection(set(ids_to_include))
            elif (search_terms_to_exclude != '') and key in ('google_article', 'google_image'):
                ids_we_can_use = set(ids).difference(set(ids_to_exclude))
            value['is_enough'] = True if len(ids_we_can_use) >= value['pins_per_day'] * days_amount else False
            value['ids_we_can_use'] = ids_we_can_use
            enough_all_pins = enough_all_pins and value['is_enough']
            if not value['is_enough']:
                pin_types_needed.append(key)

        now = datetime.now()
        days = []
        day_folder_names = []
        if enough_all_pins:
            for day in range(days_amount):
                start_date_time = now + timedelta(days=day)
                day_date = start_date_time.date()
                days.append(day_date)
                console("Creating a folder with pins for the day ", day, " - ", day_date)
                day_folder_name = os.path.join(Config.SCHEDULE_SET_FOLDER, str(day_date))
                day_folder_names.append(day_folder_name)
                if not os.path.exists(day_folder_name):
                    os.makedirs(day_folder_name)
            # move pins to he folders separated by days
            pins_to_schedule = move_pins(days_amount, day_folder_names, types_to_check)
            schedule_set_scheduler(pins_to_schedule, TIME_RANGES, TIME_RANGES_FULL)
            # then schedule pins from day_folders + schedule repins
            if repin_amount > 0:
                console('START SCHEDULE REPINS')
                schedule_re_pins(days_amount, repin_amount, repin_count)
            # starting the auto follow
            console('STARTING AUTO FOLLOW')
            console('Time ranges for auto follow - ', time_ranges_follow)
            follow(count_follow, username_followers, days_amount_follow, time_ranges_follow)
            SCHEDULE.start()
        else:
            str_to_return = "**************************************************\n" \
                            "There are not enough items of the following types:\n"
            for el in pin_types_needed:
                str_to_return += el + '\n'
            str_to_return += "**************************************************\n"
            console(str_to_return)
            return str_to_return
    else:
        console('Not all the required parameters were set. Parameters to be set:')
        console(params_result[1])


def move_pins(days_amount, day_folder_names, types_to_check):
    move_folders = {}
    global FOLDER_NAME_TO_ID

    key_counter = 0
    for key in types_to_check:
        type_el = types_to_check[key]
        if type_el['pins_per_day'] > 0:
            # copy type_el['pins_per_day'] pins into day_folder_names folders
            ids_to_use = list(type_el['ids_we_can_use'])
            pins_needed = type_el['pins_per_day'] * days_amount
            pins_per_day = type_el['pins_per_day']
            day_counter = 0
            for day in day_folder_names:
                if key_counter == 0:
                    # init move_folders
                    move_folders[day] = []
                for i in range(day_counter * pins_per_day, day_counter * pins_per_day + pins_per_day):
                    move_folders[day].append([os.path.join(type_el['folder_name'], ids_to_use[i])])
                    # FOLDER_NAME_TO_ID[os.path.join(type_el['folder_name'], ids_to_use[i])] = ids_to_use[i]
                day_counter += 1
            key_counter += 1
    # moving the folders
    for key in move_folders:
        el_to_move = move_folders[key]
        for folder in el_to_move:
            shutil.move(folder[0], key)
        date_name = key.replace(Config.SCHEDULE_SET_FOLDER, '')
        date_pin_folders = [[key + '/' + el, date_name] for el in os.listdir(key)]
        move_folders[key] = date_pin_folders
    return move_folders


def process_time_ranges(time_ranges, target_time_ranges):
    ranges = time_ranges.split(',')
    for t_range in ranges:
        hours = t_range.split('-')
        target_time_ranges.append([int(hours[0]), int(hours[1])])


def schedule_re_pins(days_amount, amount, repin_count):
    global ALL_PINS

    search_terms = database.get_search_terms()
    now = datetime.now()

    if SEARCH_TERMS_TO_INCLUDE != '':
        for search_term in SEARCH_TERMS_TO_INCLUDE_ARR:
            console('SEARCH TERM')
            console(search_term)
            get_pins_by_single_search_term(search_term, amount * days_amount, repin_count)
    elif SEARCH_TERMS_TO_EXCLUDE != '':
        for search_term in search_terms:
            if not (search_term in SEARCH_TERMS_TO_EXCLUDE_ARR):
                console(search_term)
                get_pins_by_single_search_term(search_term, amount * days_amount, repin_count)
    else:
        for search_term in search_terms:
            console('SEARCH TERM')
            console(search_term)
            get_pins_by_single_search_term(search_term, amount * days_amount, repin_count)
    schedule_activity(len(ALL_PINS), PINS_AMOUNT_MAP, TIME_RANGES, TIME_RANGES_FULL, re_pin_single, ALL_PINS,
                      days_amount)


def get_pins_by_single_search_term(search_term, count, repin_count):
    global ALL_PINS

    pins = database.get_pins_by_terms(search_term, count, repin_count)
    for pin in pins:
        ALL_PINS.append(np.array(pin))


def schedule_set_scheduler(days_map, time_to_schedule, time_to_schedule_full):
    day_number = 0
    items_map = {}
    now = datetime.now()

    console('days_map')
    console(days_map)

    for key in days_map:
        day_folder_name = key
        items_per_day = len(days_map[key])
        arguments = days_map[key]
        console('ARGUMENTS')
        console(arguments)
        items_map[key] = {}
        define_intervals(items_per_day, time_to_schedule, time_to_schedule_full, items_map[key])
        start_date_time = now + timedelta(days=day_number)
        if day_number == 0:
            schedule_for_today(start_date_time, items_per_day, 0, time_to_schedule_full,
                               items_map[key],
                               arguments, post_pin_by_folder)
        else:
            schedule_for_single_day(start_date_time, items_per_day, 0, time_to_schedule_full,
                                    items_map[key],
                                    arguments, post_pin_by_folder)

        day_number += 1


def schedule_activity(amount, target_map, time_to_schedule, time_to_schedule_full, method, arguments, days_amount):
    if SCHEDULE.running:
        SCHEDULE.pause()
    # checking how many pins we can divide for each day
    items_for_one_day = amount // days_amount
    items_to_add_for_first_day = 0
    if items_for_one_day * days_amount == amount:
        console('Found ', amount, ' items to schedule. Each day ', items_for_one_day, ' will be posted')
    else:
        items_to_add_for_first_day = amount - items_for_one_day * days_amount
        console('Found ', amount, ' items to schedule. Each day ', items_for_one_day,
                ' will be posted. For the first day there will be ', items_to_add_for_first_day, ' added')
    define_intervals(items_for_one_day, time_to_schedule, time_to_schedule_full, target_map)

    day = 0
    item_start_id = 0
    now = datetime.now()

    while day < days_amount:
        start_date_time = now + timedelta(days=day)
        if day > 0:
            schedule_for_single_day(start_date_time, items_for_one_day, item_start_id, time_to_schedule_full,
                                    target_map,
                                    arguments, method)
        elif day == 0:
            schedule_for_today(start_date_time, items_for_one_day, item_start_id, time_to_schedule_full, target_map,
                               arguments, method)

        day += 1
        item_start_id += items_for_one_day

    # scheduling rest of the pins for the first day
    if items_to_add_for_first_day > 0:
        # redefine intervals
        console("Redefining intervals for the rest of the items which will be posted for the first day")
        define_intervals(items_to_add_for_first_day, time_to_schedule, time_to_schedule_full, target_map)
        schedule_for_today(now, items_to_add_for_first_day, item_start_id, time_to_schedule_full, target_map, arguments,
                           method)


def schedule_for_today(start_date_time, items_for_one_day, item_start_id, time_to_schedule_full,
                       target_map, arguments, method):
    current_start_id = item_start_id
    for el in time_to_schedule_full:
        console(time_to_schedule_full[el], " ", target_map[el])

        i = 0

        while i < target_map[el]:
            # if (i+itemStartId in arguments):
            console('Scheduling item ', i + current_start_id)
            # define a date to be scheduled for the item
            now = start_date_time

            time_range_time_start = now.replace(hour=time_to_schedule_full[el][0], minute=0, second=0)
            time_range_time_end = now.replace(hour=time_to_schedule_full[el][1], minute=0, second=0)
            console(now, ' ', str(time_range_time_start) + ' ', str(time_range_time_end))
            console('CURRENT ARGUMENT', arguments[i + current_start_id])
            if now < time_range_time_start:
                console('now is earlier than ' + str(time_range_time_start),
                        ", scheduling the activity in the random time between " + str(time_to_schedule_full[el]))
                run_date = now.replace(hour=time_to_schedule_full[el][0], minute=randint(0, 59), second=0)
                console(run_date)
                SCHEDULE.add_job(method, 'date', run_date=run_date,
                                 args=arguments[i + current_start_id])

            elif time_range_time_start <= now < time_range_time_end:
                minute = now.minute
                # if this is the end of the time range, we SCHEDULE for an end hour of the time range, plus one minute
                if minute >= 58:
                    run_date = now.replace(hour=time_to_schedule_full[el][1], minute=1, second=0)
                else:
                    run_date = now.replace(hour=time_to_schedule_full[el][0], minute=randint(minute + 1, 59), second=0)
                    console('now is between the', time_to_schedule_full[el], ', scheduling the activity at ', run_date)
                SCHEDULE.add_job(method, 'date', run_date=run_date, args=arguments[i + current_start_id])

            elif now >= time_range_time_end:
                run_date = now.replace(hour=time_to_schedule_full[el][0], minute=randint(0, 59), second=0) + timedelta(
                    days=1)
                console('now is later than ', str(time_range_time_end),
                        ", scheduling the activity for the next day same time: ", run_date)
                SCHEDULE.add_job(method, 'date', run_date=run_date, args=arguments[i + current_start_id])

            i += 1
        current_start_id += target_map[el]


# Logic for scheduling for any day other than today
def schedule_for_single_day(start_date_time, items_for_one_day, item_start_id, time_to_schedule_full, target_map,
                            arguments, method):
    current_start_id = item_start_id
    for el in time_to_schedule_full:
        console(time_to_schedule_full[el], " ", target_map[el])

        i = 0

        while i < target_map[el]:
            # if (i+itemStartId in arguments):
            console('Scheduling item ', i + current_start_id)
            # define a date to be scheduled for the item
            now = start_date_time
            time_range_time_start = now.replace(hour=time_to_schedule_full[el][0], minute=0, second=0)
            time_range_time_end = now.replace(hour=time_to_schedule_full[el][1], minute=0, second=0)
            console(str(now), str(time_range_time_start), str(time_range_time_end))
            console('CURRENT ARGUMENT', arguments[i + current_start_id])

            console("scheduling the activity in the random time between ", str(time_to_schedule_full[el]))
            run_date = now.replace(hour=time_to_schedule_full[el][0], minute=randint(0, 59), second=0)
            SCHEDULE.add_job(func=method, trigger='date', run_date=run_date, args=arguments[i + current_start_id])

            i += 1
        current_start_id += target_map[el]


def define_intervals(count, time_to_schedule, time_to_schedule_full, target_map):
    time_ranges_count = 0
    time_items_count = 0
    time_ranges_full_count = 0

    console('time_to_schedule', time_to_schedule)
    for time_interval in time_to_schedule:
        previous_hour = None
        time_items_count += 1

        # init items map
        console('time_interval', time_interval)
        for hour in time_interval:

            if previous_hour is not None:
                # time range is time between two hours
                time_ranges_count += hour - previous_hour
                # populate actual hour-by-hour time ranges
                i = 1
                previous_hour_store = previous_hour
                while i <= hour - previous_hour:
                    time_to_schedule_full[time_ranges_full_count] = [previous_hour_store, previous_hour_store + 1]
                    time_ranges_full_count += 1
                    previous_hour_store += 1
                    i += 1

            previous_hour = hour

    console("TIME_RANGES_COUNT", time_ranges_count)
    avg_items_per_time_range = int(round(count / time_ranges_count))
    console("items_per_time_range", avg_items_per_time_range)
    items_assigned_to_intervals = avg_items_per_time_range * time_ranges_count
    console(items_assigned_to_intervals)
    console('time_to_schedule_full', time_to_schedule_full)

    # if we have too many items after rounding
    if items_assigned_to_intervals > count:
        avg_items_per_time_range = avg_items_per_time_range - 1
        items_assigned_to_intervals = avg_items_per_time_range * time_ranges_count
        console('items_assigned_to_intervals', items_assigned_to_intervals)
    # populate intervals pith amounts of items
    i = 0
    while i < time_ranges_count:
        target_map[i] = avg_items_per_time_range
        i += 1

    if items_assigned_to_intervals != count:
        items_to_add_amount = count - items_assigned_to_intervals
        # check_all_items = 0
        i = 0
        while i < items_to_add_amount:
            map_id = randint(0, time_ranges_count - 1)
            target_map[map_id] += 1
            i = i + 1
    console('target_map: ', target_map)


# def difference(list1, list2):
#     list_dif = [i for i in list1 + list2 if i not in list1 or i not in list2]
#     print('PINS TO CHECK', len(list_dif))
#     return list_dif


def insert_pins():
    pin_data = read_dir()
    # Check whether the pins is on the publication today
    if pin_data is not None:
        for key in pin_data:
            pin = pin_data[key]
            pin_id = str(uuid.uuid4())
            os.rename(pin.get('folderName'), Config.UPLOAD_MY_PINS_FOLDER + pin_id)
            try:
                col_board_ids = pin.get("jsonData").get('col_board_ids')
                Pin(search_term='my_pins', board_id=pin.get("jsonData").get("board_id"),
                    col_board_ids=','.join(col_board_ids),
                    pin_id=pin_id, type_pin='ownPin', is_unique=1, link=pin.get("jsonData").get("link"),
                    title=pin.get("jsonData").get("title")[:Config.PIN_TITLE_MAX_LENGTH],
                    description=pin.get("jsonData").get("description")[:Config.PIN_DESCR_MAX_LENGTH - 3] + '...',
                    status=0).insert()
            except:
                console('There was an error while reading own pins. Check the pin folder ', key)


def set_boards():
    # Get all own boards
    boards = pinterest.boards(username=pinterest.username, page_size=100)
    for board in boards:
        Board(name=board.get('name'), board_id=board.get('id'),
              is_collaboration=board.get('is_collaborative') if 1 else 0).insert()
    return get_boards()


def get_boards():
    result = []
    for board in Board.query.all():
        result.append({"board_id": board.board_id, "name": board.name,
                       "is_collaborative": board.is_collaboration})
    return result


def read_dir():
    path = Config.UPLOAD_MY_PINS_FOLDER
    result = {}
    for entry in os.listdir(path):
        if not entry == '.DS_Store':
            if os.path.isdir(os.path.join(path, entry)):
                image_path = ""
                json_data = ""
                for pin in os.listdir(Config.UPLOAD_MY_PINS_FOLDER + entry):
                    if pin.endswith('.json'):
                        json_data = json.loads(open(Config.UPLOAD_MY_PINS_FOLDER + entry + '/' + pin, "r").read())
                    else:
                        image_path = str(Config.UPLOAD_MY_PINS_FOLDER + entry + '/' + pin)

                result[Config.UPLOAD_MY_PINS_FOLDER + entry] = {'imagePath': image_path, 'jsonData': json_data,
                                                                'folderName': Config.UPLOAD_MY_PINS_FOLDER + entry}
    return result


def post_pin_by_folder(folder_name, date_name):
    console('POSTING THE PIN ', folder_name)
    image_path = ""
    json_data = ""
    # define pin's metadata and image
    for el in os.listdir(folder_name):
        if el.endswith('.json'):
            json_data = json.loads(open(os.path.join(folder_name, el), "r").read())
            console(json_data)
        else:
            image_path = str(folder_name + '/' + el)
    pin_response = pinterest.upload_pin(
        section_id=None,
        image_file=image_path,
        link=json_data.get("link"),
        title=json_data.get("title"),
        board_id=json_data.get("board_id"),
        description=json_data.get("description"),
    )
    response_data = json.loads(pin_response.content)
    pin_id = response_data["resource_response"]["data"]["id"]
    # Update pin in Database
    pin_id_folder_name = folder_name.replace(Config.SCHEDULE_SET_FOLDER + date_name + '/', '')
    Pin(pin_id=pin_id_folder_name).update(dict(status=1))
    posted_folder_name = Config.POSTED_PINS_FOLDER + date_name
    utils.create_dir(posted_folder_name)
    shutil.move(folder_name, posted_folder_name)
    col_boards_ids = []
    # Start rePin
    col_boards_ids = json_data.get("col_board_ids")
    console('Collaborative board Ids')
    console(col_boards_ids)
    if POST_TO_COLLABORATIVE_BOARDS and col_boards_ids != '':
        for board in col_boards_ids:
            request = pinterest.repin(board_id=board, pin_id=pin_id)
            if request.status_code != 200:
                console("There was a problem while repinning a pin ", pin_id, " to the collaborative board",
                        board.board_id)

    return response_data


def copy_dir(folder_name, symlinks=False, ignore=None):
    from_path = Path().absolute().joinpath(f"uploads/pins/{folder_name}")
    to_path = Path().absolute().joinpath(f"uploads/all pins/{folder_name}")

    if not os.path.exists(to_path):
        os.makedirs(to_path)
    for item in os.listdir(from_path):
        s = os.path.join(from_path, item)
        d = os.path.join(to_path, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
                shutil.copy2(s, d)

    delete_dir(from_path)
    console(f"Folder | {folder_name} | was be copied!")
    return f"Folder | {folder_name} | was be copied!"


def delete_dir(folder_name):
    path = Path().absolute().joinpath(f"uploads/pins/{folder_name}")
    if os.path.isdir(path):
        shutil.rmtree(path)
        console(f"Folder | {folder_name} | was be deleted!")
    else:
        console(f"Folder | {folder_name} | not exist and has not been deleted!")
