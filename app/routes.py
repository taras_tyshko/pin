import os
from datetime import datetime
from threading import Thread

from flask import render_template, request, jsonify
from werkzeug.utils import secure_filename
from app import app, pin, socketio, console
from app.models import User
from app.services import database, quote, utils
from app.services.google_article_search import search
from app.services.google_search import search_results
from app.config import Config
from git import Repo

THREAD_SEARCH = Thread()
THREAD_SCHEDULE_SET = Thread()
THREAD_GOOGLE_ARTICLE = Thread()
THREAD_GOOGLE_IMAGE = Thread()


@app.route('/', methods=['POST', 'GET', 'OPTIONS'])
@app.route('/index', methods=['POST', 'GET', 'OPTIONS'])
def index():
    with open(Config.LOG_FILE, 'r') as f:
        log_buffer = f.readlines()
    return render_template('stream.html', log_buffer=log_buffer)


def message_received():
    console('message was received!!!')


@socketio.on('my event')
def handle_logs(json):
    console('received my event: ' + str(json))
    with open(Config.LOG_FILE, 'r') as f:
        log_buffer = f.readlines()
        log_buffer.reverse()

    socketio.emit('my response', log_buffer[0], callback=message_received)


@app.route('/log')
def log():
    console('Function Logging is Working!')
    return render_template('index.html')


@app.route('/credentials')
def credentials():
    email = request.args.get(key='email', type=str)
    username = request.args.get(key='username', type=str)
    password = request.args.get(key='password', type=str)
    return User(password=password, email=email, username=username).insert()


@app.route('/search', methods=["GET"])
def pin_search():
    # resetting the params
    Config.STOP_THREAD = False

    query = request.args.get(key='query', type=str)
    board_id = request.args.get(key='board_id', type=str)
    re_pin_count = request.args.get(key='rePinCount', type=int)
    global THREAD_SEARCH
    THREAD_SEARCH = Thread(target=pin.search, args=(query, re_pin_count, board_id))
    THREAD_SEARCH.daemon = True
    THREAD_SEARCH.start()
    THREAD_SEARCH.join()
    if not Config.STOP_THREAD:
        commit()
    return "Pinterest search finished!"


@app.route('/google_article_search', methods=["GET"])
def google_article_search():
    # resetting the params
    Config.STOP_THREAD = False

    query = request.args.get(key='query', type=str)
    count = request.args.get(key='count', type=int)
    board_id = request.args.get(key='board_id', type=str)
    # col_board_ids = request.args.get(key='col_board_ids', type=str).strip()
    col_board_ids_req = request.args.get(key='col_board_ids', type=str)
    col_board_ids = col_board_ids_req.strip() if col_board_ids_req is not None else ''
    description_prefix = request.args.get(key='description_prefix', type=str)
    google_descr_length = request.args.get(key='google_descr_length', type=int)
    # search(query, count, board_id, col_board_ids, description_prefix, google_descr_length)
    global THREAD_GOOGLE_ARTICLE
    THREAD_GOOGLE_ARTICLE = Thread(target=search, args=(
        query, count, board_id, col_board_ids, description_prefix, google_descr_length))
    THREAD_GOOGLE_ARTICLE.daemon = True
    THREAD_GOOGLE_ARTICLE.start()
    THREAD_GOOGLE_ARTICLE.join()
    if not Config.STOP_THREAD:
        commit()
    return "Google article search finished!"


@app.route('/google_images_search', methods=["GET"])
def google_images_search():
    # resetting the params
    Config.STOP_THREAD = False

    query = request.args.get(key='query', type=str)
    count = request.args.get(key='count', type=int)
    board_id = request.args.get(key='board_id', type=str)
    # col_board_ids = request.args.get(key='col_board_ids', type=str).strip()
    col_board_ids_req = request.args.get(key='col_board_ids', type=str)
    col_board_ids = col_board_ids_req.strip() if col_board_ids_req is not None else ''
    description_prefix = request.args.get(key='description_prefix', type=str)
    # search_results(query, board_id, col_board_ids, count, description_prefix)
    global THREAD_GOOGLE_IMAGE
    THREAD_GOOGLE_IMAGE = Thread(target=search_results,
                                 args=(query, board_id, col_board_ids, count, description_prefix))
    THREAD_GOOGLE_IMAGE.daemon = True
    THREAD_GOOGLE_IMAGE.start()
    THREAD_GOOGLE_IMAGE.join()
    # thread = Thread(target=google_article_search.search, args=(query, count, pictures_count, board_id, description_prefix, google_descr_length))
    # thread.daemon = True
    # thread.start()
    if not Config.STOP_THREAD:
        commit()
    return "Google images search finished!"


@app.route("/search_terms", methods=["GET"])
def search_terms():
    console("Test")
    return jsonify(database.counts_pins())


@app.route("/stop_all_jobs", methods=["GET"])
def stop_all_jobs():
    pin.stop_scheduled_jobs()
    return "You have stopped all scheduled jobs"


@app.route("/boards", methods=["GET"])
def boards():
    username = request.args.get(key='username', type=str)
    return jsonify(pin.list_board_ids(username))


@app.route("/local_fonts", methods=["GET"])
def local_fonts():
    return {'List of loaded fonts': quote.read_fonts()}


@app.route("/schedule_set", methods=["GET"])
def schedule_set():
    # amount of pins means amount of pins to post taken from each category for each day
    days_amount = request.args.get(key='daysAmount for pins', type=int)
    repin_amount = request.args.get(key='Repin amount for 1 day', type=int)
    repin_count = request.args.get(key='repin_count', type=int)
    google_articles_count = request.args.get(key='Google articles amount for 1 day', type=int)
    google_images_count = request.args.get(key='Google images amount for 1 day', type=int)
    quotes_count = request.args.get(key='Quotes amount for 1 day', type=int)
    my_pins_count = request.args.get(key='My pins amount for 1 day', type=int)
    search_terms_to_include = request.args.get(key='search_terms_to_include', type=str).strip()
    search_terms_to_exclude = request.args.get(key='search_terms_to_exclude', type=str).strip()
    time_ranges = request.args.get(key='Time ranges for scheduler', type=str).strip()
    count_follow = request.args.get(key='Count for followers', type=int)
    days_amount_follow = request.args.get(key='daysAmount for follows', type=int)
    username_followers = request.args.get(key='Username for followers', type=str)
    time_ranges_follow = request.args.get(key='Time ranges for follow', type=str).strip()
    post_to_coll = request.args.get(key='Post to collaborative boards', type=bool)
    global THREAD_SCHEDULE_SET
    THREAD_SCHEDULE_SET = Thread(target=pin.schedule_set,
                                 args=(days_amount,
                                       repin_amount, repin_count,
                                       google_articles_count,
                                       google_images_count,
                                       quotes_count,
                                       my_pins_count,
                                       search_terms_to_include, search_terms_to_exclude,
                                       time_ranges,
                                       count_follow, days_amount_follow, username_followers, time_ranges_follow,
                                       post_to_coll))
    THREAD_SCHEDULE_SET.daemon = False
    THREAD_SCHEDULE_SET.start()
    THREAD_SCHEDULE_SET.join()
    if not Config.STOP_THREAD:
        commit()
    return "Pinterest SCHEDULE SET has finished!"


@app.route("/stop_all", methods=["GET"])
def stop_all():
    global THREAD_SEARCH
    global THREAD_SCHEDULE_SET
    global THREAD_GOOGLE_IMAGE
    global THREAD_GOOGLE_ARTICLE
    if THREAD_SCHEDULE_SET.is_alive():
        pin.stop_scheduled_jobs()
    if THREAD_SEARCH.is_alive():
        console('Stopping the search')
    if THREAD_GOOGLE_IMAGE.is_alive():
        console('Stopping google images search')
    if THREAD_GOOGLE_ARTICLE.is_alive():
        console('Stopping google article search')
    Config.STOP_THREAD = True
    return 'All scheduled jobs have been stopped'


@app.route('/uploads', methods=['POST'])
def uploads():
    # Before using this route please define that are you would like doing.
    # param key ['logo'] -> uploads logo
    # param key ['fonts'] -> uploads fonts
    # param key ['quotes'] -> uploads quotes
    # param key ['my_pins'] -> uploads my pins

    # check if the post request has the file part
    keys = ['quotes', 'logo', 'fonts', 'my_pins']
    key = ''

    for e in keys:
        if e in request.files:
            key += e

    if key is '':
        return f'You use no valid download key. Please use this: {keys}'

    file = request.files[key]
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        return 'No selected file. Please choose file!'
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        path = os.path.join(define_folder_path(key), filename)
        file.save(path)

        if key is 'quotes':
            quote.draw_quotes(path)

        if key is 'my_pins':
            utils.zip_extract(path, filename)
            pin.insert_pins()

        commit()
        return f'File | {filename} | has been uploaded!'
    else:
        return f'You can only download these files in these extensions: {Config.ALLOWED_FILE_EXTENSIONS}'


# @app.route("/schedule_repin", methods=["GET"])
# def schedule_re_pins():
#     # amount of pins means amount of pins to post taken from each category for each day
#     amount = request.args.get(key='amount', type=int)
#     days_amount = request.args.get(key='daysAmount', type=int)
#     repin_count = request.args.get(key='repin_count', type=int)
#     search_terms_to_include = request.args.get(key='search_terms_to_include', type=str).strip()
#     search_terms_to_exclude = request.args.get(key='search_terms_to_exclude', type=str).strip()
#     thread = Thread(target=pin.schedule_re_pins,
#                     args=(days_amount, amount, repin_count, search_terms_to_include, search_terms_to_exclude))
#     thread.daemon = True
#     thread.start()
#     return "Pinterest SCHEDULE Re Pins has been started!"


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in Config.ALLOWED_FILE_EXTENSIONS


def define_folder_path(key):
    if key == 'quotes':
        return Config.UPLOAD_TMP_FOLDER
    elif key == 'logo':
        return Config.UPLOAD_LOGO_FOLDER
    elif key == 'fonts':
        return Config.UPLOAD_FONTS_FOLDER
    elif key == 'my_pins':
        return Config.UPLOAD_TMP_FOLDER
    else:
        return Config.UPLOAD_FOLDER


def commit():
    global THREAD_SEARCH
    global THREAD_SCHEDULE_SET
    global THREAD_GOOGLE_IMAGE
    global THREAD_GOOGLE_ARTICLE
    is_alive_search = THREAD_SEARCH.isAlive()
    is_alive_schedule = THREAD_SCHEDULE_SET.isAlive()
    is_alive_google_article = THREAD_GOOGLE_ARTICLE.isAlive()
    is_alive_google_image = THREAD_GOOGLE_IMAGE.isAlive()
    while is_alive_search or is_alive_schedule or is_alive_google_article or is_alive_google_image:
        if not THREAD_SEARCH.isAlive():
            is_alive_search = False
        if not THREAD_SCHEDULE_SET.isAlive():
            is_alive_schedule = False
        if not THREAD_GOOGLE_ARTICLE.isAlive():
            is_alive_google_article = False
        if not THREAD_GOOGLE_IMAGE.isAlive():
            is_alive_google_image = False
    if not (is_alive_schedule or is_alive_search or is_alive_google_article or is_alive_google_image):
        # commit
        repo = Repo(path=Config.ROOT_PATH)
        repo.git.pull()
        repo.git.add('--all')
        repo.git.commit(m=f"Server commit | Changes: {str(datetime.now())}")
        repo.git.push()
    return 'All the changes have been committed!'
