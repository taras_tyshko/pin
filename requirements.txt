alembic==1.4.2
APScheduler==3.6.3
autoenv==1.0.0
beautifulsoup4==4.9.0
cachetools==4.1.0
certifi==2020.4.5.1
chardet==3.0.4
click==7.1.2
colorama==0.4.3
fake-useragent==0.1.11
Flask==1.1.2
Flask-Cors==3.0.8
Flask-Migrate==2.5.3
Flask-SocketIO==4.3.0
Flask-SQLAlchemy==2.4.1
Flask-Threads==0.0.1a0
future==0.18.2
gevent==20.5.0
gitdb==4.0.5
GitPython==3.1.2
google==2.0.3
google-api-core==1.17.0
google-api-python-client==1.8.2
google-auth==1.14.1
google-auth-httplib2==0.0.3
Google-Images-Search==1.1.0
googleapis-common-protos==1.51.0
greenlet==0.4.15
gunicorn==20.0.4
httplib2==0.17.3
idna==2.9
itsdangerous==1.1.0
Jinja2==2.11.2
Mako==1.1.2
MarkupSafe==1.1.1
numpy==1.18.3
Pillow==6.2.2
pinterest-api==0.0.8
protobuf==3.11.3
py3-pinterest==1.0.1
pyasn1==0.4.8
pyasn1-modules==0.2.8
pyfiglet==0.8.post1
python-dateutil==2.8.1
python-dotenv==0.13.0
python-editor==1.0.4
python-engineio==3.12.1
python-resize-image==1.1.19
python-socketio==4.5.1
pytz==2019.3
requests==2.23.0
requests-toolbelt==0.9.1
rsa==4.0
selenium==3.141.0
six==1.14.0
smmap==3.0.4
soupsieve==2.0
SQLAlchemy==1.3.16
termcolor==1.1.0
tzlocal==2.0.0
Unidecode==1.1.1
uritemplate==3.0.1
urllib3==1.25.9
uWSGI==2.0.18
Werkzeug==1.0.1
